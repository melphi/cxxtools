

HEADERS += \
    $$PWD/LogMod.h \
    $$PWD/StreamParser.h \
    $$PWD/ThreadPool.h \
    $$PWD/Utils.h \
    $$PWD/netaddress.h\
    $$PWD/tcpsocket.h\
    $$PWD/udpsocket.h

SOURCES += \
    $$PWD/LogMod.cpp \
    $$PWD/StreamParser.cpp \
    $$PWD/netaddress.cpp\
    $$PWD/tcpsocket.cpp\
    $$PWD/udpsocket.cpp

win32{
LIBS += -lws2_32 -liphlpapi
}
