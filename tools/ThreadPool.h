#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <vector>
#include <queue>
#include <atomic>
#include <future>
#include <stdexcept>

namespace tool {
 
#define  THREADPOOL_MAX_NUM 16
//#define  THREADPOOL_AUTO_GROW

//线程池,可以提交变参函数或拉姆达表达式的匿名函数执行,可以获取执行返回值
//不直接支持类成员函数, 支持类静态成员函数或全局函数,Opterator()函数等
class ThreadPool
{
    using Task = std::function<void()>;    //定义类型
public:
    inline ThreadPool(unsigned short size = 4) { addThread(size); }

    inline ~ThreadPool()
    {
        m_run=false;
        m_task_cv.notify_all(); // 唤醒所有线程执行
        for (auto& thread : m_pool) {
            //thread.detach(); // 让线程“自生自灭”
            if(thread.joinable())
                thread.join(); // 等待任务结束， 前提：线程一定会执行完
        }
    }

public:
    // 提交一个任务
    // 调用.get()获取返回值会等待任务执行完,获取返回值
    // 有两种方法可以实现调用类成员，
    // 一种是使用   bind： .commit(std::bind(&Dog::sayHello, &dog));
    // 一种是用   mem_fn： .commit(std::mem_fn(&Dog::sayHello), this)
    template<class F, class... Args>
    auto AddTask(F&& f, Args&&... args) ->std::future<decltype(f(args...))>
    {
        if (!m_run)    // stoped ??
            throw std::runtime_error("commit on ThreadPool is stopped.");

        using RetType = decltype(f(args...)); // typename std::result_of<F(Args...)>::type, 函数 f 的返回值类型
        auto task = std::make_shared<std::packaged_task<RetType()>>(
            std::bind(std::forward<F>(f), std::forward<Args>(args)...)
        ); // 把函数入口及参数,打包(绑定)
        std::future<RetType> future = task->get_future();
        {    // 添加任务到队列
            std::lock_guard<std::mutex> lock{ m_lock };//对当前块的语句加锁  lock_guard 是 mutex 的 stack 封装类，构造的时候 lock()，析构的时候 unlock()
            m_tasks.emplace([task](){ // push(Task{...}) 放到队列后面
                (*task)();
            });
        }
#ifdef THREADPOOL_AUTO_GROW
        if (m_idlThrNum < 1 && m_pool.size() < THREADPOOL_MAX_NUM)
            addThread(1);
#endif // !THREADPOOL_AUTO_GROW
        m_task_cv.notify_one(); // 唤醒一个线程执行

        return future;
    }

    //空闲线程数量
    int IdleCount() { return m_idlThrNum; }
    //线程数量
    int ThreadCount() { return m_pool.size(); }


#ifndef THREADPOOL_AUTO_GROW
private:
#endif // !THREADPOOL_AUTO_GROW
    //添加指定数量的线程
    void addThread(uint16_t size)
    {
        for (; m_pool.size() < THREADPOOL_MAX_NUM && size > 0; --size)
        {   //增加线程数量,但不超过 预定义数量 THREADPOOL_MAX_NUM
            m_pool.emplace_back( [this]{ //工作线程函数
                while (m_run)
                {
                    Task task; // 获取一个待执行的 task
                    {
                        // uniquem_lock 相比 lock_guard 的好处是：可以随时 unlock() 和 lock()
                        std::unique_lock<std::mutex> lock{ m_lock };
                        m_task_cv.wait(lock, [this]{
                                return !m_run || !m_tasks.empty();
                        }); // wait 直到有 task
                        if (!m_run && m_tasks.empty())
                            return;
                        task = move(m_tasks.front()); // 按先进先出从队列取一个 task
                        m_tasks.pop();
                    }
                    m_idlThrNum--;
                    task();//执行任务
                    m_idlThrNum++;
                }
            });
            m_idlThrNum++;
        }
    }

private:
    std::vector<std::thread> m_pool;     //线程池
    std::queue<Task> m_tasks;            //任务队列
    std::mutex m_lock;                   //同步
    std::condition_variable m_task_cv;   //条件阻塞
    std::atomic<bool> m_run = ATOMIC_VAR_INIT(true);     //线程池是否执行
    std::atomic<int>  m_idlThrNum = ATOMIC_VAR_INIT(0);  //空闲线程数量
};

}//namespace tool


#endif // THREADPOOL_H
