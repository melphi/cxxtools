#ifndef UDPSOCKET_H
#define UDPSOCKET_H

#include "netaddress.h"
#include "Utils.h"
#include <functional>

namespace tool{ namespace net{

using DataListener = std::function<void(const char* data,int size)>;


class UdpSenderPrivate;
class UdpSender
{
public:
    DEFAULT_MOVE(UdpSender)

    UdpSender(const std::string& destAddr, uint16_t destPort,uint32_t localIp = 0u);
    UdpSender(const NetAddress& na,uint32_t localIp = 0u);
    ~UdpSender();

    void notifyForQuit();//prepare for closing inline thread
    int sendData(const char *buff, const int len) const;
    const char* addr() const;
    uint16_t port() const;
private:
    UdpSenderPrivate*   d;
    DISABLE_COPY(UdpSender)
};

class UdpReceiverPrivate;
class UdpReceiver
{
public:
    DEFAULT_MOVE(UdpReceiver)

    UdpReceiver(DataListener pListener,uint32_t localIp, const std::string& groupAddr, uint16_t groupPort,int recBuffSize = 1024);
    UdpReceiver(DataListener pListener,uint32_t localIp,const NetAddress& na,int recBuffSize = 1024);
    ~UdpReceiver();

    void notifyForQuit();//prepare for closing inline thread
    const char* addr() const;
    uint16_t port() const;
private:
    UdpReceiverPrivate*   d;
    DISABLE_COPY(UdpReceiver)
};


}}//namespace tool::net

#endif // UDPSOCKET_H
