#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include "netaddress.h"
#include "Utils.h"
#include <cstdint>
#include <functional>

namespace tool {
namespace net {

using DataListener = std::function<void(const char* data, int size)>;
using ConnListener = std::function<void(NetAddress addr, bool state)>;

class TcpServerPrivate;
class TcpServer
{
public:
    DEFAULT_MOVE(TcpServer)

    TcpServer(DataListener pListener, ConnListener pConnector);
    ~TcpServer();
    void notifyForQuit();
    bool connect(const std::string& addr, uint16_t port, int maxClientNum);
    bool connect(const NetAddress& na, int maxClientNum);
    void disconnect();
    bool isConnected() const;
    //send data to all client
    void sendData(const char* data, uint32_t size);
private:
    TcpServerPrivate* d;
    DISABLE_COPY(TcpServer)
};




class TcpClientPrivate;
class TcpClient
{
public:
    DEFAULT_MOVE(TcpClient)

    TcpClient(const std::string& addr, uint16_t port, DataListener pListener);
    TcpClient(const NetAddress& na, DataListener pListener);
    ~TcpClient();

    void notifyForQuit();
    bool isConnected() const;
    bool sendData(const char* data, uint32_t size);
    const char* addr() const;
    uint16_t port() const;
private:
    TcpClientPrivate* d;
    DISABLE_COPY(TcpClient)
};



}
}//namepsace net::tool

#endif // TCPSOCKET_H
