#pragma once

namespace tool {
namespace Log {
class CSysLog
{
public:
	using MessageCallback = std::function<void(const char*, int)>;
	static CSysLog* GetInstance()
	{
		static CSysLog s_instance;
		return &s_instance;
	}
	~CSysLog();

	void ShowMessage(const char* szFormat, ...) CHECK_FORMAT(2, 3);

	void ShowMessage(int nLogLevelIn, const char* functionName, int line, const char* szFormat, ...) CHECK_FORMAT(5, 6);

	void setScreenState(bool isOpen);//default is open

	void setFileState(bool isOpen, std::string fileName = "");//default is close

	void installMessageCallback(MessageCallback callback);//custom message process function
private:
	CSysLog();
	DISABLE_COPY(CSysLog)

	FILE*			m_file;
	std::mutex      m_mutex;
	bool            m_toScreen;
	bool            m_toFile;
	MessageCallback m_callback;
};//CSysLog


}}//namespace tool::Log